let express = require(`express`);
let mongoose = require(`mongoose`);

let app = express();
const PORT = 5000;

//require from router

let taskRoutes = require(`./routes/taskRoutes`);


app.use(express.json());
app.use(express.urlencoded({extended:true}));


mongoose.connect(`mongodb+srv://zuittDBBatch125:70pez0265@cluster0.l6bhm.mongodb.net/s31?retryWrites=true&w=majority`,
	{
		useNewUrlParser:true,
		useUnifiedTopology:true
	}).then(()=> {
		console.log(`Connected to Database.`);
	}).catch((error)=> {
		console.log(error);
	});

app.use(`/`,taskRoutes);
app.listen(PORT,()=>console.log(`Server is running on port ${PORT}`));

