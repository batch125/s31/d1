
const User = require('./../models/User');

//insert a new user
module.exports.createUser = function(reqBody){
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		userName: reqBody.userName,
		password: reqBody.password
	});

	return newUser.save().then( (savedUser, error) => {
		if(error){
			return error
		} 
		else {
			return savedUser
		}
	});
}

//get all users
module.exports.getUsers = () => {
	return User.find().then( (error, result)=> {
		if(error){
			return error;
		} 
		else {
			return result;
		}
	});
}


/*module.exports.getUserData = () => {
	let params = req.params.id
	return User.findById(params, (error, result)=> {
			if(error){
				return error;
			} else {
				return result;
			}
		});

}*/

module.exports.getUserData = (params) => {

	
	return User.findById(params).then( (result) => {
	return result
	})
}

module.exports.updateUser = (params, reqBody) =>{

	return User.findByIdAndUpdate(params,reqBody,{new:true}).then((error, result)=> {
		if (error) {
			return error
		} else {
			return result
		}
	})
		
}

module.exports.deleteUser = (params, reqBody) =>{

	return User.findByIdAndDelete(params).then((error, result)=> {
		if (error) {
			return error
		} else {
			return result
		}
	})
		
}



