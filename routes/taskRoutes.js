let express = require(`express`);
let router = express.Router();
let User = require(`./../models/User`);
let userController = require(`./../controllers/userControllers`);

router.post(`/add_user`,(req,res)=>{
	// console.log(req.body);
	/*let newUser = new User({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		userName: req.body.userName,
		password: req.body.password
	})

	newUser.save((error,savedUser)=>{
		if(error){
			res.send(error)
		}
		else {
			res.send(`New user is saved`,savedUser)
		}
	})*/
	userController.createUser(req.body).then(result=>res.send(result));
})

router.get('/users', (req, res)=> {
	/*User.find({}, (error, result)=> {
		if(error){
			console.log(error);
		} else {
			res.send(result);
		}
	});*/

	userController.getUsers().then(result=>res.send(result));
});


router.get('/users/:id', (req, res)=> {
	let params = req.params.id
	/*User.findById(params, (error, result)=> {
		if(error){
			console.log(error);
		} else {
			res.send(result);
		}
	});*/

	userController.getUserData(params).then( result => res.send(result));
	// userController.getUserData().then(result=>res.send(result));
});

router.put('/user/:id', (req, res)=> {
	let params = req.params.id
	/*User.findByIdAndUpdate(params,req.body,{new:true},(error, result)=> {
		if(error){
			console.log(error);
		} else {
			res.send(`User has been updated ${result}`);
		}
	});*/

	userController.updateUser(params, req.body).then(result => res.send(result))
});

router.delete('/user/:id', (req, res)=> {
	let params = req.params.id
	/*User.findByIdAndDelete(params,(error, result)=> {
		if(error){
			console.log(error);
		} else {
			res.send(true);
		}
	});*/

	userController.deleteUser(params).then(result => res.send(result))
});

module.exports = router;